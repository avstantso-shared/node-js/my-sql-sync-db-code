# avstantso: MySql code generator by information_schema tables

Generate code for MySql database information_schema.ROUTINES (procs & funcs) and information_schema.VIEWS. For use with @avstantso/node-js--my-sql-wrapper