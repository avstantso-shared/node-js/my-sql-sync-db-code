const { Types, typeFrom } = require('./typeMap');
const defaultBoolFuncsPatterns = require('./defaultBoolFuncsPatterns');

const paramsDef = (params, forceTypes) =>
  !params
    ? []
    : params.map(({ name, type }) => {
        const t = typeFrom(type, name, forceTypes);
        return `${name}: ${'any' === t ? `Arg` : `Arg<${t}>`}`;
      });

const paramsSet = (params) => (!params ? [] : params.map(({ name }) => name));

const funcGenConv = (name, resultType, forceTypes) => {
  if (
    'tinyint' === resultType &&
    (undefined === forceTypes?.boolFuncs
      ? defaultBoolFuncsPatterns
      : forceTypes.boolFuncs
    ).find((regExp) => regExp.test(name))
  )
    return {
      genDef: undefined,
      conv: 'bool',
    };

  if (Types.Date.includes(resultType))
    return {
      genDef: undefined,
      conv: 'date',
    };

  if (Types.Json.includes(resultType))
    return {
      gen: 'T',
      genDef: 'Func.ReturnType',
      conv: undefined,
    };

  return {
    gen: `${typeFrom(resultType)}`,
    genDef: undefined,
    conv: undefined,
  };
};

module.exports = { paramsDef, paramsSet, funcGenConv };
