const { TS } = require('@avstantso/node-js--code-gen');
const { paramsDef, paramsSet, funcGenConv } = require('./params');

module.exports = (routines, routineType, forceTypes) => {
  const isFunc = 'FUNCTION' === routineType;
  const className = isFunc ? 'Func' : 'Proc';

  return TS.resolve([
    TS.Import.Members([className, 'Arg']).From(
      '@avstantso/node-js--my-sql-wrapper'
    ),
    TS.Import.Members('schemaNameEsc').From('./schema'),
    '\r\n',
    routines.map(({ name, params }) => {
      let resultType;
      const paramsExceptResult = !isFunc
        ? params
        : params.filter(({ name, type }) => {
            if (!name) resultType = type;
            return name;
          });

      const { gen, genDef, conv } = !isFunc
        ? {}
        : funcGenConv(name, resultType, forceTypes);

      return TS.Export.Function(
        name,
        isFunc && genDef ? `T extends Func.ReturnType = ${genDef}` : '',
        paramsDef(paramsExceptResult, forceTypes).join(','),
        '',
        TS.Return(
          TS.Call.Gen(
            isFunc ? `${className}.${conv || 'raw'}` : TS.New(className),
            gen,
            [
              `${'`${schemaNameEsc}'}.${'$'}{${name}.name}${'`'}`,
              ...paramsSet(paramsExceptResult),
            ]
          )
        )
      );
    }),
  ]);
};
