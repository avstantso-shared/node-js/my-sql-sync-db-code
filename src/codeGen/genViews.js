const path = require('path');
const fs = require('fs');
const Promise = require('bluebird');
const { writeSourceFile, TS } = require('@avstantso/node-js--code-gen');

const INT_TYPES = ['TINYINT', 'INT', 'BIGINT'];
const NO_LENGTH_TYPES = [
  ...INT_TYPES,
  'DECIMAL',
  'JSON',
  'TIMESTAMP',
  'DATETIME',
  'DATE',
  'TEXT',
  'MEDIUMTEXT',
  'LONGTEXT',
  'JSON',
];
const FSP_TYPES = ['TIMESTAMP', 'DATETIME'];
const ALL_TYPES = [
  'TINYINT',
  'INT',
  'BIGINT',
  'BOOL',
  'DECIMAL',
  'CHAR',
  'VARCHAR',
  'TEXT',
  'MEDIUMTEXT',
  'LONGTEXT',
  'JSON',
  'BINARY',
  'VARBINARY',
  'TIMESTAMP',
  'DATETIME',
  'DATE',
];

function fields2Defs(fields, forceTypes) {
  return fields.map(({ name, type, cml: size, dtd }) => {
    let t = type.toUpperCase();
    let s = NO_LENGTH_TYPES.includes(t) ? '' : `${size}`;

    if (INT_TYPES.includes(t) && (forceTypes?.boolean || []).includes(name)) {
      t = `BOOL`;
      s = '';
    } else if (FSP_TYPES.includes(t)) {
      const m = /(?<=\()\d+(?=\))/.exec(dtd);
      if (m) s = m[0];
    }

    return { name, type: t, toString: () => `${name}: ${t}(${s})` };
  });
}

function fieldsDefs2Import(fieldsDefs) {
  return fieldsDefs
    .reduce((r, { type }) => (r.includes(type) || r.push(type)) && r, [])
    .sort((a, b) => ALL_TYPES.indexOf(a) - ALL_TYPES.indexOf(b));
}

module.exports = async (
  schemaFolder,
  scriptName,
  views,
  forceTypes,
  silent
) => {
  const viewsFolder = path.resolve(schemaFolder, 'views');
  if (!fs.existsSync(viewsFolder)) fs.mkdirSync(viewsFolder);

  await Promise.mapSeries(views, async ({ name, fields }) => {
    const fieldsDefs = fields2Defs(fields, forceTypes);

    const source = TS.resolve(
      [
        TS.Import.Members('mysqlTypes, View').From(
          '@avstantso/node-js--my-sql-wrapper'
        ),
        TS.Import.Members('schemaNameEsc').From('../schema'),

        TS.Const(
          TS.Object(fieldsDefs2Import(fieldsDefs).join(',')),
          'mysqlTypes'
        ),

        TS.Const('fields', TS.Object(fieldsDefs.join(',\r\n')) + '\r\n'),

        TS.Export.Namespace(name, TS.Export.Type('Fields', 'typeof fields')),

        TS.Export.Const(
          name,
          TS.New(
            TS.Call.Gen('View', `${name}.Fields`, [
              `${'`${schemaNameEsc}'}.${name}${'`'}`,
              'fields',
            ])
          )
        ),
      ],
      '\r\n'
    );

    await writeSourceFile(
      path.resolve(viewsFolder, `${name}.ts`),
      source,
      scriptName,
      { silent }
    );
  });

  const indexSource = views
    .map(({ name }) => TS.Export.Asterisk.From(`./${name}`))
    .join('');

  await writeSourceFile(
    path.resolve(viewsFolder, `index.ts`),
    indexSource,
    scriptName,
    { silent }
  );
};
