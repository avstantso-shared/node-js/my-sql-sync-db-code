const Types = {
  string: ['char', 'varchar', 'text', 'mediumtext', 'longtext', 'json'],
  number: ['tinyint', 'int', 'bigint', 'decimal'],
  Buffer: ['binary', 'varbinary'],
  Date: ['timestamp', 'datetime', 'date'],
  Json: ['json'],
};

const typeFrom = (type, name, forceTypes) => {
  if (Types.string.includes(type)) return 'string';

  if (Types.number.includes(type)) {
    if ((forceTypes?.boolean || []).includes(name)) return 'boolean';

    return 'number';
  }

  if (Types.Buffer.includes(type)) return 'Buffer';

  if (Types.Date.includes(type)) return 'Date';

  throw Error(`Unknown type "${type}" for name "${name}"`);
};

module.exports = { Types, typeFrom };
