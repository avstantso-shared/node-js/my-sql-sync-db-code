const path = require('path');
const fs = require('fs');
const Promise = require('bluebird');
const { writeSourceFile, TS } = require('@avstantso/node-js--code-gen');
const { readMetaData } = require('../db');

const genRoutines = require('./genRoutines');
const genViews = require('./genViews');

const defaultBoolFuncsPatterns = require('./defaultBoolFuncsPatterns');

const CodeGen = (generatorOptions = { silent: false }) => {
  const { silent } = generatorOptions;

  const defaultOptions = {
    schemaEnv: undefined,
    indexTemplate: {
      sysFunctionsPath: '../sysFunctions',
    },
    forceTypes: {
      boolean: [
        'active',
        'system',
        'confirmed',
        'important',
        'a_active',
        'a_confirmed',
        'a_condition',
        'a_replace',
        'a_important',
      ],
      boolFuncs: defaultBoolFuncsPatterns,
    },
  };

  const generate = async (
    mysqlPoolOrConnection,
    defaultSchemaName,
    folder,
    scriptName,
    options = defaultOptions
  ) => {
    const { schemaEnv, indexTemplate, forceTypes } = options;

    const schemaName =
      (schemaEnv && process.env[schemaEnv]) || defaultSchemaName;

    const meta = await readMetaData(mysqlPoolOrConnection, schemaName);

    if (!fs.existsSync(folder)) fs.mkdirSync(folder);
    const schemaFolder = path.resolve(folder, defaultSchemaName);
    if (!fs.existsSync(schemaFolder)) fs.mkdirSync(schemaFolder);

    await writeSourceFile(
      path.resolve(schemaFolder, 'schema.ts'),
      TS.Import.Members('escape').From('@avstantso/node-js--my-sql-wrapper') +
        `\r\n` +
        TS.Export.Const(
          'schemaName',
          `${schemaEnv ? `process.env.${schemaEnv} ||` : ''}'${schemaName}'`
        ) +
        TS.Export.Const('schemaNameEsc', 'escape(schemaName)'),
      scriptName,
      { silent }
    );

    const [hasFuncs, hasProcs] = await Promise.mapSeries(
      [
        {
          file: 'functions.ts',
          type: 'FUNCTION',
        },
        {
          file: 'procedures.ts',
          type: 'PROCEDURE',
        },
      ],
      async ({ type, file }) => {
        const routines = meta.routines.filter(
          (routine) => routine.type === type
        );

        if (!routines.length) return false;

        const fileName = path.resolve(schemaFolder, file);
        const source = genRoutines(routines, type, forceTypes);
        await writeSourceFile(fileName, source, scriptName, { silent });

        return true;
      }
    );

    const hasViews = !!meta.views.length;
    if (hasViews)
      await genViews(schemaFolder, scriptName, meta.views, forceTypes, silent);

    await writeSourceFile(
      path.resolve(schemaFolder, 'index.ts'),
      () => {
        // const hasSysF = fs.existsSync(path.resolve(folder, 'sysFunctions'));

        const { sysFunctionsPath } = indexTemplate;

        return TS.resolve([
          sysFunctionsPath &&
            (sysFunctionsPath.startsWith('.')
              ? TS.Import.AsteriskAs
              : TS.Import.Members)('sysFunctions').From(sysFunctionsPath),
          TS.Import.Members('schemaName, schemaNameEsc').From('./schema'),
          hasFuncs &&
            TS.Import.AsteriskAs('schemaFunctions').From('./functions'),
          hasProcs && TS.Import.AsteriskAs('Procedures').From('./procedures'),
          hasViews && TS.Import.AsteriskAs('Views').From('./views'),
          '\r\n',
          (sysFunctionsPath || hasFuncs) &&
            TS.Const(
              'Functions',
              hasFuncs
                ? sysFunctionsPath
                  ? '{...sysFunctions, ...schemaFunctions}'
                  : 'schemaFunctions'
                : 'sysFunctions'
            ),
          '\r\n',
          TS.Export.Object(
            sysFunctionsPath && 'sysFunctions',
            'schemaName',
            'schemaNameEsc',
            (sysFunctionsPath || hasFuncs) && 'Functions',
            hasProcs && 'Procedures',
            hasViews && 'Views'
          ),
        ]);
      },
      scriptName,
      { silent }
    );
  };

  return { defaultOptions, generate };
};

module.exports = CodeGen;
