const fs = require('fs');
const Connector = require('./connector');

const loadSql = (file) => `${fs.readFileSync(`${__dirname}/sql/${file}.sql`)}`;

const sql = {
  routines: loadSql('routines'),
  views: loadSql('views'),
};

async function isDBExists(conn, schemaName) {
  const [[resObj]] = await conn.query(`SHOW DATABASES LIKE '${schemaName}';`);

  return resObj && !!Object.keys(resObj).length;
}

const readMetaData = async (mysqlPoolOrConnection, schemaName) => {
  const { doInConnection } = Connector(mysqlPoolOrConnection);

  const raw = await doInConnection(async (conn) => {
    if (!(await isDBExists(conn, schemaName)))
      throw Error(`Database "${schemaName}" not exist`);

    const [routines] = await conn.query(sql.routines, schemaName);
    const [views] = await conn.query(sql.views, schemaName);

    return { routines, views };
  });

  const routines = raw.routines.map(({ data }) => data);
  routines.forEach((r) => r.params?.sort((a, b) => a.index - b.index));

  const views = raw.views.map(({ data }) => data);
  views.forEach((r) => r.fields.sort((a, b) => a.index - b.index));

  return { routines, views };
};

module.exports = { readMetaData };
