const Connector = (mysqlPoolOrConnection) => {
  const isPool = !!mysqlPoolOrConnection.getConnection;
  let innerConnection;

  const connect = async () => {
    if (isPool) {
      innerConnection = await mysqlPoolOrConnection.getConnection();
      return innerConnection;
    } else return mysqlPoolOrConnection;
  };

  const release = () => innerConnection && innerConnection.release();

  const doInConnection = async (callback) => {
    const connection = await connect();
    try {
      return callback(connection);
    } finally {
      release();
    }
  };

  return { connect, release, doInConnection };
};

module.exports = Connector;
