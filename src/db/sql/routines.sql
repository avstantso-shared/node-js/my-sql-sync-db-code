SELECT
  JSON_OBJECT(
		'type', r.ROUTINE_TYPE, 
		'name', r.SPECIFIC_NAME,
		'params', 
		(
			SELECT
    JSON_ARRAYAGG( 
					JSON_OBJECT(
						'index', p.ORDINAL_POSITION,
						'name', p.PARAMETER_NAME,
						'mode', p.PARAMETER_MODE,
						'type', p.DATA_TYPE,
						'cml', p.CHARACTER_MAXIMUM_LENGTH,
						'col', p.CHARACTER_OCTET_LENGTH,
						'np', p.NUMERIC_PRECISION,
						'ns', p.NUMERIC_SCALE,
						'dp', p.DATETIME_PRECISION,
						'dtd', p.DTD_IDENTIFIER
				)
			)
  FROM information_schema.PARAMETERS as p
  WHERE r.ROUTINE_SCHEMA = p.SPECIFIC_SCHEMA
    AND r.ROUTINE_NAME = p.SPECIFIC_NAME
  ORDER BY p.ORDINAL_POSITION
		)            
	) as data
FROM information_schema.ROUTINES as r
WHERE r.ROUTINE_SCHEMA=?
ORDER BY r.ROUTINE_TYPE, r.SPECIFIC_NAME
