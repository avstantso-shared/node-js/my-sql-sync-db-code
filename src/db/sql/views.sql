SELECT
	JSON_OBJECT(
		'name', v.TABLE_NAME,
		'fields', 
		(
			SELECT
		JSON_ARRAYAGG( 
					JSON_OBJECT(
						'index', c.ORDINAL_POSITION,
						'name', c.COLUMN_NAME,
						'type', c.DATA_TYPE,
						'cml', c.CHARACTER_MAXIMUM_LENGTH,
						'col', c.CHARACTER_OCTET_LENGTH,
						'np', c.NUMERIC_PRECISION,
						'ns', c.NUMERIC_SCALE,
						'dp', c.DATETIME_PRECISION,
						'dtd', c.COLUMN_TYPE
				)
			)
	FROM information_schema.COLUMNS as c
	WHERE v.TABLE_SCHEMA = c.TABLE_SCHEMA
		AND v.TABLE_NAME = c.TABLE_NAME
	ORDER BY c.ORDINAL_POSITION
		)            
	) as data
FROM information_schema.VIEWS as v
WHERE v.TABLE_SCHEMA=?
ORDER BY v.TABLE_NAME
